module gitea.com/${REPO_OWNER}/${REPO_NAME}

go 1.15

require code.gitea.io/sdk/gitea v0.15.0

require github.com/hashicorp/go-version v1.2.1 // indirect
